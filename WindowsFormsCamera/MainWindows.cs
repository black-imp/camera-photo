﻿using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace WindowsFormsCamera
{
    public partial class MainWindows : Form
    {
        // 相机设备
        private FilterInfoCollection Cameras = null;
        
        // 相机连接数量
        private int CameraNumber = 0;

        // 打开相机按钮状态
        private bool OpenCameraStart = false;

        // 相机的设备信息
        private string Camera1Info = null;
        private string Camera2Info = null;
        private string Camera3Info = null;
        private string Camera4Info = null;
        private string Camera5Info = null;
        private string Camera6Info = null;

        private VideoCaptureDevice CameraDevice1 = null;
        private VideoCaptureDevice CameraDevice2 = null;
        private VideoCaptureDevice CameraDevice3 = null;
        private VideoCaptureDevice CameraDevice4 = null;
        private VideoCaptureDevice CameraDevice5 = null;
        private VideoCaptureDevice CameraDevice6 = null;

        public MainWindows()
        {
            InitializeComponent();
        }

        // load事件，用于检测连接当前设备的相机
        private void MainWindows_Load(object sender, EventArgs e)
        {
            DetectionCameras();
        }

        private void MainWindows_Closing(object sender, FormClosingEventArgs e)
        {
            this.CloseCameras();
        }

        // 检测当前设备建立连接的相机
        private void DetectionCameras()
        {

            this.PrintMessage("程序启动，检测当前设备连接的相机数........");

            try
            {
                // 枚举当前设备建立连接的相机
                Cameras = new FilterInfoCollection(FilterCategory.VideoInputDevice);

                comboBox1.Items.Add("---");
                comboBox2.Items.Add("---");
                comboBox3.Items.Add("---");
                comboBox4.Items.Add("---");
                comboBox5.Items.Add("---");
                comboBox6.Items.Add("---");

                if (Cameras.Count == 0)
                {
                    throw new ApplicationException();
                }

                foreach(FilterInfo infos in Cameras)
                {
                    comboBox1.Items.Add(infos.Name);
                    comboBox2.Items.Add(infos.Name);
                    comboBox3.Items.Add(infos.Name);
                    comboBox4.Items.Add(infos.Name);
                    comboBox5.Items.Add(infos.Name);
                    comboBox6.Items.Add(infos.Name);

                    this.PrintMessage(infos.Name + " 连接到设备...");

                    CameraNumber++;
                }


                comboBox1.SelectedIndex = 0;
                comboBox2.SelectedIndex = 0;
                comboBox3.SelectedIndex = 0;
                comboBox4.SelectedIndex = 0;
                comboBox5.SelectedIndex = 0;
                comboBox6.SelectedIndex = 0;

                switch (CameraNumber)
                {
                    case 1:
                        // 拷贝出相机1信息
                        Camera1Info = Cameras[0].MonikerString;
                        break;

                    case 2:
                        // 拷贝出相机1信息
                        Camera1Info = Cameras[0].MonikerString;
                        // 拷贝出相机2信息
                        Camera2Info = Cameras[1].MonikerString;
                        break;

                    case 3:
                        // 拷贝出相机1信息
                        Camera1Info = Cameras[0].MonikerString;
                        // 拷贝出相机2信息
                        Camera2Info = Cameras[1].MonikerString;
                        // 拷贝出相机3信息
                        Camera3Info = Cameras[2].MonikerString;
                        break;

                    case 4:
                        // 拷贝出相机1信息
                        Camera1Info = Cameras[0].MonikerString;
                        // 拷贝出相机2信息
                        Camera2Info = Cameras[1].MonikerString;
                        // 拷贝出相机3信息
                        Camera3Info = Cameras[2].MonikerString;
                        // 拷贝出相机4信息
                        Camera4Info = Cameras[3].MonikerString;
                        break;

                    case 5:
                        // 拷贝出相机1信息
                        Camera1Info = Cameras[0].MonikerString;
                        // 拷贝出相机2信息
                        Camera2Info = Cameras[1].MonikerString;
                        // 拷贝出相机3信息
                        Camera3Info = Cameras[2].MonikerString;
                        // 拷贝出相机4信息
                        Camera4Info = Cameras[3].MonikerString;
                        // 拷贝出相机5信息
                        Camera5Info = Cameras[4].MonikerString;
                        break;

                    case 6:
                        // 拷贝出相机1信息
                        Camera1Info = Cameras[0].MonikerString;
                        // 拷贝出相机2信息
                        Camera2Info = Cameras[1].MonikerString;
                        // 拷贝出相机3信息
                        Camera3Info = Cameras[2].MonikerString;
                        // 拷贝出相机4信息
                        Camera4Info = Cameras[3].MonikerString;
                        // 拷贝出相机5信息
                        Camera5Info = Cameras[4].MonikerString;
                        // 拷贝出相机6信息
                        Camera6Info = Cameras[5].MonikerString;
                        break;

                    default:
                        break;
                }

                // 将相机设置控件、拍照保存控件设置为不可以操作
                this.SavePicture.Enabled = false;
                this.SetCamera1.Enabled = false;
                this.SetCamera2.Enabled = false;
                this.SetCamera3.Enabled = false;
                this.SetCamera4.Enabled = false;
                this.SetCamera5.Enabled = false;
                this.SetCamera6.Enabled = false;
                this.comboBox1.Enabled = false;
                this.comboBox2.Enabled = false;
                this.comboBox3.Enabled = false;
                this.comboBox4.Enabled = false;
                this.comboBox5.Enabled = false;
                this.comboBox6.Enabled = false;

                this.PrintMessage("当前设备连接的相机数 : " + CameraNumber);
                
            }
            catch (ApplicationException)
            {
                this.comboBox1.Items.Add("No find camera");
                this.comboBox2.Items.Add("No find camera");
                this.comboBox3.Items.Add("No find camera");
                this.comboBox4.Items.Add("No find camera");
                this.comboBox5.Items.Add("No find camera");
                this.comboBox6.Items.Add("No find camera");

                this.comboBox1.SelectedIndex = 0;
                this.comboBox2.SelectedIndex = 0;
                this.comboBox3.SelectedIndex = 0;
                this.comboBox4.SelectedIndex = 0;
                this.comboBox5.SelectedIndex = 0;
                this.comboBox6.SelectedIndex = 0;
                Cameras = null;

                // 将界面的所有控件设置为不可操作
                this.openCameras.Enabled = false;
                this.SavePicture.Enabled = false;
                this.SetCamera1.Enabled = false;
                this.SetCamera2.Enabled = false;
                this.SetCamera3.Enabled = false;
                this.SetCamera4.Enabled = false;
                this.SetCamera6.Enabled = false;
                this.SetCamera5.Enabled = false;
                this.SavePath.Enabled = false;
                this.comboBox1.Enabled = false;
                this.comboBox2.Enabled = false;
                this.comboBox3.Enabled = false;
                this.comboBox4.Enabled = false;
                this.comboBox5.Enabled = false;
                this.comboBox6.Enabled = false;
                this.SavePathText.Enabled = false;

                this.PrintMessage("当前设备未检测到相机，请检查相机的连接，重启软件！");

                MessageBox.Show("当前设备未检测到相机，请检查相机的连接，重启软件！", "Error: ");
            }
        }

        private void OpenCameras()
        {
            this.PrintMessage("打开当前设备建立连接的相机....");

            // 打开相机1
            if (Camera1Info != null)
            {
                CameraDevice1 = new VideoCaptureDevice(Camera1Info);
                //CameraDevice1.DesiredFrameSize = new Size(344, 257);   // 设置大小
                //CameraDevice1.DesiredFrameRate = 1;  // 设置帧率
                videoSourcePlayer1.VideoSource = CameraDevice1;
                videoSourcePlayer1.Start();

                this.comboBox1.SelectedIndex = 1;
                this.SetCamera1.Enabled = true;
                this.SetCamera2.Enabled = false;
                this.SetCamera3.Enabled = false;
                this.SetCamera4.Enabled = false;
                this.SetCamera5.Enabled = false;
                this.SetCamera6.Enabled = false;

                this.PrintMessage("相机(" + Camera1Info + ")，打开成功!");
            }

            // 打开相机2
            if (Camera2Info != null)
            {
                CameraDevice2 = new VideoCaptureDevice(Camera2Info);
                //CameraDevice2.DesiredFrameSize = new Size(344, 257);   // 设置大小
                //CameraDevice2.DesiredFrameRate = 1;  // 设置帧率
                videoSourcePlayer2.VideoSource = CameraDevice2;
                videoSourcePlayer2.Start();

                this.comboBox2.SelectedIndex = 2;
                this.SetCamera1.Enabled = true;
                this.SetCamera2.Enabled = true;
                this.SetCamera3.Enabled = false;
                this.SetCamera4.Enabled = false;
                this.SetCamera5.Enabled = false;
                this.SetCamera6.Enabled = false;

                this.PrintMessage("相机(" + Camera2Info + ")，打开成功!");
            }

            //打开相机3*******************************
            if (Camera3Info != null)
            {
                CameraDevice3 = new VideoCaptureDevice(Camera3Info);
                //CameraDevice3.DesiredFrameSize = new Size(344, 257);   // 设置大小
                //CameraDevice3.DesiredFrameRate = 1;  // 设置帧率
                videoSourcePlayer3.VideoSource = CameraDevice3;
                videoSourcePlayer3.Start();

                this.comboBox3.SelectedIndex = 3;
                this.SetCamera1.Enabled = true;
                this.SetCamera2.Enabled = true;
                this.SetCamera3.Enabled = true;
                this.SetCamera4.Enabled = false;
                this.SetCamera5.Enabled = false;
                this.SetCamera6.Enabled = false;

                this.PrintMessage("相机(" + Camera3Info + ")，打开成功!");
            }

            // 打开相机4
            if (Camera4Info != null)
            {
                CameraDevice4 = new VideoCaptureDevice(Camera4Info);
                //CameraDevice4.DesiredFrameSize = new Size(344, 257);   // 设置大小
                //CameraDevice4.DesiredFrameRate = 1;  // 设置帧率
                videoSourcePlayer4.VideoSource = CameraDevice4;
                videoSourcePlayer4.Start();

                this.comboBox4.SelectedIndex = 4;
                this.SetCamera1.Enabled = true;
                this.SetCamera2.Enabled = true;
                this.SetCamera3.Enabled = true;
                this.SetCamera4.Enabled = true;
                this.SetCamera5.Enabled = false;
                this.SetCamera6.Enabled = false;

                this.PrintMessage("相机(" + Camera4Info + ")，打开成功!");
            }

            // 打开相机5
            if (Camera5Info != null)
            {
                CameraDevice5 = new VideoCaptureDevice(Camera5Info);
                //CameraDevice5.DesiredFrameSize = new Size(344, 257);   // 设置大小
                //CameraDevice5.DesiredFrameRate = 1;  // 设置帧率
                videoSourcePlayer5.VideoSource = CameraDevice5;
                videoSourcePlayer5.Start();

                this.comboBox5.SelectedIndex = 5;
                this.SetCamera1.Enabled = true;
                this.SetCamera2.Enabled = true;
                this.SetCamera3.Enabled = true;
                this.SetCamera4.Enabled = true;
                this.SetCamera5.Enabled = true;
                this.SetCamera6.Enabled = false;

                this.PrintMessage("相机(" + Camera5Info + ")，打开成功!");
            }

            // 打开相机6
            if (Camera6Info != null)
            {
                CameraDevice6 = new VideoCaptureDevice(Camera6Info);
                //CameraDevice6.DesiredFrameSize = new Size(344, 257);   // 设置大小
                //CameraDevice6.DesiredFrameRate = 1;  // 设置帧率
                videoSourcePlayer6.VideoSource = CameraDevice6;
                videoSourcePlayer6.Start();

                this.comboBox6.SelectedIndex = 6;
                this.SetCamera1.Enabled = true;
                this.SetCamera2.Enabled = true;
                this.SetCamera3.Enabled = true;
                this.SetCamera4.Enabled = true;
                this.SetCamera5.Enabled = true;
                this.SetCamera6.Enabled = true;

                this.PrintMessage("相机(" + Camera6Info + ")，打开成功!");
            }

            this.comboBox1.Enabled = true;
            this.comboBox2.Enabled = true;
            this.comboBox3.Enabled = true;
            this.comboBox4.Enabled = true;
            this.comboBox5.Enabled = true;
            this.comboBox6.Enabled = true;
        }

        // 关闭相机操作
        private void CloseCameras()
        {
            // 先判断相机是否建立连接运行
            if (videoSourcePlayer1 != null && videoSourcePlayer1.IsRunning)
            {
                try
                {
                    // 关闭相机的两个方式
                    videoSourcePlayer1.SignalToStop();
                    videoSourcePlayer1.WaitForStop();

                    this.PrintMessage("关闭相机[" + Camera1Info + "]");
                }
                catch
                {
                    this.PrintMessage(Camera1Info + "相机关闭失败!");
                    MessageBox.Show(Camera1Info + "相机关闭失败!", "Error:");
                }
            }

            if (videoSourcePlayer2 != null && videoSourcePlayer2.IsRunning)
            {
                try
                {
                    // 关闭相机的两个方式
                    videoSourcePlayer2.SignalToStop();
                    videoSourcePlayer2.WaitForStop();

                    this.PrintMessage("关闭相机[" + Camera2Info + "]");
                }
                catch
                {
                    this.PrintMessage(Camera2Info + "相机关闭失败!");
                    MessageBox.Show(Camera2Info + "相机关闭失败!", "Error:");
                }
            }

            if (videoSourcePlayer3 != null && videoSourcePlayer3.IsRunning)
            {
                try
                {
                    // 关闭相机的两个方式
                    videoSourcePlayer3.SignalToStop();
                    videoSourcePlayer3.WaitForStop();

                    this.PrintMessage("关闭相机[" + Camera3Info + "]");
                }
                catch
                {
                    this.PrintMessage(Camera3Info + "相机关闭失败!");
                    MessageBox.Show(Camera3Info + "相机关闭失败!", "Error:");
                }
            }

            if (videoSourcePlayer4 != null && videoSourcePlayer4.IsRunning)
            {
                try
                {
                    // 关闭相机的两个方式
                    videoSourcePlayer4.SignalToStop();
                    videoSourcePlayer4.WaitForStop();

                    this.PrintMessage("关闭相机[" + Camera4Info + "]");
                }
                catch
                {
                    this.PrintMessage(Camera4Info + "相机关闭失败!");
                    MessageBox.Show(Camera4Info + "相机关闭失败!", "Error:");
                }
            }

            if (videoSourcePlayer5 != null && videoSourcePlayer5.IsRunning)
            {
                try
                {
                    // 关闭相机的两个方式
                    videoSourcePlayer5.SignalToStop();
                    videoSourcePlayer5.WaitForStop();

                    this.PrintMessage("关闭相机[" + Camera5Info + "]");
                }
                catch
                {
                    this.PrintMessage(Camera5Info + "相机关闭失败!");
                    MessageBox.Show(Camera5Info + "相机关闭失败!", "Error:");
                }
            }

            if (videoSourcePlayer6 != null && videoSourcePlayer6.IsRunning)
            {
                try
                {
                    // 关闭相机的两个方式
                    videoSourcePlayer6.SignalToStop();
                    videoSourcePlayer6.WaitForStop();

                    this.PrintMessage("关闭相机[" + Camera6Info + "]");
                }
                catch
                {
                    this.PrintMessage(Camera6Info + "相机关闭失败!");
                    MessageBox.Show(Camera6Info + "相机关闭失败!", "Error:");
                }
            }
        }

        // 信息打印
        private void PrintMessage(string str)
        {
            if (str != null)
            {
                this.RecordText.AppendText("[ " + System.DateTime.Now.ToString("F") + "]  " + str + "\r\n");
            }
        }


        private void openCameras_Click(object sender, EventArgs e)
        {
            if(!OpenCameraStart)
            {
                this.OpenCameras();
                this.openCameras.Text = "关闭相机";

                this.OpenCameraStart = true;

                this.SavePicture.Enabled = true;
            }
            else
            {
                this.CloseCameras();
                this.openCameras.Text = "打开相机";

                this.OpenCameraStart = false;

                this.SavePicture.Enabled = false;

                this.SetCamera1.Enabled = false;
                this.SetCamera2.Enabled = false;
                this.SetCamera3.Enabled = false;
                this.SetCamera4.Enabled = false;
            }
        }

        private void SavePath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog ChooseSaveDialog = new FolderBrowserDialog();
            ChooseSaveDialog.Description = "选择保存照片的路径";

            if(ChooseSaveDialog.ShowDialog() == DialogResult.OK)
            {
                string FilePath = ChooseSaveDialog.SelectedPath;
                this.SavePathText.Text = FilePath;

                if (this.OpenCameraStart)
                {
                    this.SavePicture.Enabled = true;
                }

                this.PrintMessage("保存路径设置为: " + FilePath);
            }
        }


        private void SavePicture_Click(object sender, EventArgs e)
        {
            try
            {
                // 相机1拍照保存
                if (videoSourcePlayer1 != null && videoSourcePlayer1.IsRunning)
                {
                    Bitmap bitmap1 = videoSourcePlayer1.GetCurrentVideoFrame();
                    string fileName1 = SavePathText.Text + string.Format(@"\\{0}_1.jpg", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                    // 将抓捕的图片写入文件
                    bitmap1.Save(fileName1);

                    this.PrintMessage("照片保存成功! " + fileName1);
                }

                // 相机2拍照保存
                if (videoSourcePlayer2 != null && videoSourcePlayer2.IsRunning)
                {
                    Bitmap bitmap2 = videoSourcePlayer2.GetCurrentVideoFrame();
                    string fileName2 = SavePathText.Text + string.Format(@"\\{0}_2.jpg", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                    // 将抓捕的图片写入文件
                    bitmap2.Save(fileName2);

                    this.PrintMessage("照片保存成功! " + fileName2);
                }

                // 相机3拍照保存
                if (videoSourcePlayer3 != null && videoSourcePlayer3.IsRunning)
                {

                    Bitmap bitmap3 = videoSourcePlayer3.GetCurrentVideoFrame();
                    string fileName3 = SavePathText.Text + string.Format(@"\\{0}_3.jpg", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                    // 将抓捕的图片写入文件
                    bitmap3.Save(fileName3);

                    this.PrintMessage("照片保存成功! " + fileName3);
                }

                // 相机4拍照保存
                if (videoSourcePlayer4 != null && videoSourcePlayer4.IsRunning)
                {

                    Bitmap bitmap4 = videoSourcePlayer4.GetCurrentVideoFrame();
                    string fileName4 = SavePathText.Text + string.Format(@"\\{0}_4.jpg", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                    // 将抓捕的图片写入文件
                    bitmap4.Save(fileName4);

                    this.PrintMessage("照片保存成功! " + fileName4);
                }

                // 相机5拍照保存
                if (videoSourcePlayer5 != null && videoSourcePlayer5.IsRunning)
                {

                    Bitmap bitmap5 = videoSourcePlayer5.GetCurrentVideoFrame();
                    string fileName5 = SavePathText.Text + string.Format(@"\\{0}_5.jpg", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                    // 将抓捕的图片写入文件
                    bitmap5.Save(fileName5);

                    this.PrintMessage("照片保存成功! " + fileName5);
                }

                // 相机6拍照保存
                if (videoSourcePlayer6 != null && videoSourcePlayer6.IsRunning)
                {

                    Bitmap bitmap6 = videoSourcePlayer6.GetCurrentVideoFrame();
                    string fileName6 = SavePathText.Text + string.Format(@"\\{0}_6.jpg", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                    // 将抓捕的图片写入文件
                    bitmap6.Save(fileName6);

                    this.PrintMessage("照片保存成功! " + fileName6);
                }
            }
            catch
            {
                this.PrintMessage("照片保存失败，请重试。");
                MessageBox.Show("照片保存失败！请重试。", "Error: ");
            }
        }

        private void SetCamera1_Click(object sender, EventArgs e)
        {
            CameraDevice1.DisplayPropertyPage(IntPtr.Zero);

            //CameraDevice1.Stop();
            //CameraDevice1.DisplayPropertyPage(IntPtr.Zero);
            //CameraDevice1.Start();

            this.PrintMessage("相机1被设置....");
        }

        private void SetCamera2_Click(object sender, EventArgs e)
        {
            // 1CameraDevice2.DisplayPropertyPage(IntPtr.Zero);

            CameraDevice2.Stop();
            CameraDevice2.DisplayPropertyPage(IntPtr.Zero);
            CameraDevice2.Start();

            this.PrintMessage("相机2被设置....");
        }

        private void SetCamera3_Click(object sender, EventArgs e)
        {
            CameraDevice3.DisplayPropertyPage(IntPtr.Zero);

            //CameraDevice3.Stop();
            //CameraDevice3.DisplayPropertyPage(IntPtr.Zero);
            //CameraDevice3.Start();

            this.PrintMessage("相机3被设置....");
        }

        private void SetCamera4_Click(object sender, EventArgs e)
        {
            CameraDevice4.DisplayPropertyPage(IntPtr.Zero);

            //CameraDevice4.Stop();
            //CameraDevice4.DisplayPropertyPage(IntPtr.Zero);
            //CameraDevice4.Start();

            this.PrintMessage("相机4被设置....");
        }

        private void SetCamera5_Click(object sender, EventArgs e)
        {
            CameraDevice5.DisplayPropertyPage(IntPtr.Zero);

            //CameraDevice5.Stop();
            //CameraDevice5.DisplayPropertyPage(IntPtr.Zero);
            //CameraDevice5.Start();

            this.PrintMessage("相机5被设置....");
        }

        private void SetCamera6_Click(object sender, EventArgs e)
        {
            CameraDevice6.DisplayPropertyPage(IntPtr.Zero);

            //CameraDevice6.Stop();
            //CameraDevice6.DisplayPropertyPage(IntPtr.Zero);
            //CameraDevice6.Start();

            this.PrintMessage("相机6被设置....");
        }

        private void VideoShow_Click(object sender, EventArgs e)
        {
            if(videoSourcePlayer1.GetCurrentVideoFrame() == null)
            {
                this.PrintMessage("相机1videoSourcePlayer1.GetCurrentVideoFrame() == null，无法实施显示。");
            }

            if (videoSourcePlayer2.GetCurrentVideoFrame() == null)
            {
                this.PrintMessage("相机1videoSourcePlayer2.GetCurrentVideoFrame() == null，无法实施显示。");
            }

            if (videoSourcePlayer3.GetCurrentVideoFrame() == null)
            {
                this.PrintMessage("相机1videoSourcePlayer3.GetCurrentVideoFrame() == null，无法实施显示。");
            }

            if (videoSourcePlayer4.GetCurrentVideoFrame() == null)
            {
                this.PrintMessage("相机1videoSourcePlayer4.GetCurrentVideoFrame() == null，无法实施显示。");
            }

            if (videoSourcePlayer5.GetCurrentVideoFrame() == null)
            {
                this.PrintMessage("相机1videoSourcePlayer5.GetCurrentVideoFrame() == null，无法实施显示。");
            }

            if (videoSourcePlayer6.GetCurrentVideoFrame() == null)
            {
                this.PrintMessage("相机1videoSourcePlayer6.GetCurrentVideoFrame() == null，无法实施显示。");
            }

            CameraDevice6 = new VideoCaptureDevice(Camera5Info);
            CameraDevice6.DesiredFrameSize = new Size(344, 257);   // 设置大小
            CameraDevice6.DesiredFrameRate = 1;  // 设置帧率
            videoSourcePlayer6.VideoSource = CameraDevice6;
            videoSourcePlayer6.Start();
        }

        private void RecordText_TextChange(object sender, EventArgs e)
        {
            this.RecordText.SelectionStart = this.RecordText.Text.Length;
            this.RecordText.ScrollToCaret();
        }

        //// 实时监测usb插拔状态
        //protected override void WndProc(ref Message m)
        //{
        //    try
        //    {
        //        if (m.WParam.ToInt32() == 7)
        //        {
        //            int number = 0;
        //            string[] name = new string[8];
        //            FilterInfoCollection temp = new FilterInfoCollection(FilterCategory.VideoInputDevice);

        //            if(Cameras == null)
        //            {
        //                return;
        //            }

        //            foreach (FilterInfo infos in Cameras)
        //            {
        //                name[number] = infos.Name;
        //                number++;
        //            }

        //            if (temp.Count == 0)
        //            {
        //                this.openCameras_Click(null, null);
        //                this.PrintMessage("当前设备与所有视频输入设备的连接断开! 连接数量[" + number + "]");
        //                MessageBox.Show("当前设备与视频输入设备的连接断开! 请重新建立连接并重启应用。", "Error");
        //                this.DetectionCameras();

        //                return;
        //            }

        //            if(number == CameraNumber)
        //            {
        //                return;
        //            }
        //            else if(number < CameraNumber && number > 0)
        //            {
        //                this.openCameras_Click(null, null);
        //                this.PrintMessage("当前设备与视频输入设备的连接断开! 请重新建立连接并打开相机。连接数量[" + number + "]");
        //                MessageBox.Show("当前设备与视频输入设备的连接断开! 请重新建立连接并打开相机。", "Error");
        //                this.DetectionCameras();
        //            }
        //            else
        //            {
        //                this.openCameras_Click(null, null);
        //                this.PrintMessage("当前设备与视频输入设备的连接断开! 请重新建立连接并打开相机。连接数量[" + number + "]");
        //                MessageBox.Show("当前设备有新的视频输入设备的连接! 请重新打开相机。", "Prompt");
        //                this.DetectionCameras();
        //            }


        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //    base.WndProc(ref m);
        //}

        // TODO: 下拉框选择打开相机，暂时未实现
    }
}
