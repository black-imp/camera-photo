﻿namespace WindowsFormsCamera
{
    partial class MainWindows
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Camera1Label1 = new System.Windows.Forms.Label();
            this.Camera1Label2 = new System.Windows.Forms.Label();
            this.Camera1Label3 = new System.Windows.Forms.Label();
            this.Camera1Label4 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.VideoShow = new System.Windows.Forms.Button();
            this.StopGraphic = new System.Windows.Forms.Button();
            this.StartGraphic = new System.Windows.Forms.Button();
            this.OperationCamera = new System.Windows.Forms.Label();
            this.SavePicture = new System.Windows.Forms.Button();
            this.openCameras = new System.Windows.Forms.Button();
            this.ChooseCamera = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.Camera1Label5 = new System.Windows.Forms.Label();
            this.Camera1Label6 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.SetSavePath = new System.Windows.Forms.Label();
            this.SavePathText = new System.Windows.Forms.RichTextBox();
            this.SavePath = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.SetCamera = new System.Windows.Forms.Label();
            this.SetCamera4 = new System.Windows.Forms.Button();
            this.SetCamera5 = new System.Windows.Forms.Button();
            this.SetCamera6 = new System.Windows.Forms.Button();
            this.SetCamera2 = new System.Windows.Forms.Button();
            this.SetCamera3 = new System.Windows.Forms.Button();
            this.SetCamera1 = new System.Windows.Forms.Button();
            this.RecordText = new System.Windows.Forms.RichTextBox();
            this.videoSourcePlayer1 = new AForge.Controls.VideoSourcePlayer();
            this.videoSourcePlayer2 = new AForge.Controls.VideoSourcePlayer();
            this.videoSourcePlayer4 = new AForge.Controls.VideoSourcePlayer();
            this.videoSourcePlayer5 = new AForge.Controls.VideoSourcePlayer();
            this.Camera1 = new System.Windows.Forms.Label();
            this.Camera2 = new System.Windows.Forms.Label();
            this.Camera3 = new System.Windows.Forms.Label();
            this.Camera4 = new System.Windows.Forms.Label();
            this.videoSourcePlayer3 = new AForge.Controls.VideoSourcePlayer();
            this.videoSourcePlayer6 = new AForge.Controls.VideoSourcePlayer();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Camera1Label1
            // 
            this.Camera1Label1.AutoSize = true;
            this.Camera1Label1.Location = new System.Drawing.Point(32, 48);
            this.Camera1Label1.Name = "Camera1Label1";
            this.Camera1Label1.Size = new System.Drawing.Size(41, 12);
            this.Camera1Label1.TabIndex = 0;
            this.Camera1Label1.Text = "相机1:";
            // 
            // Camera1Label2
            // 
            this.Camera1Label2.AutoSize = true;
            this.Camera1Label2.Location = new System.Drawing.Point(32, 86);
            this.Camera1Label2.Name = "Camera1Label2";
            this.Camera1Label2.Size = new System.Drawing.Size(41, 12);
            this.Camera1Label2.TabIndex = 0;
            this.Camera1Label2.Text = "相机2:";
            // 
            // Camera1Label3
            // 
            this.Camera1Label3.AutoSize = true;
            this.Camera1Label3.Location = new System.Drawing.Point(32, 124);
            this.Camera1Label3.Name = "Camera1Label3";
            this.Camera1Label3.Size = new System.Drawing.Size(41, 12);
            this.Camera1Label3.TabIndex = 0;
            this.Camera1Label3.Text = "相机3:";
            // 
            // Camera1Label4
            // 
            this.Camera1Label4.AutoSize = true;
            this.Camera1Label4.Location = new System.Drawing.Point(32, 166);
            this.Camera1Label4.Name = "Camera1Label4";
            this.Camera1Label4.Size = new System.Drawing.Size(41, 12);
            this.Camera1Label4.TabIndex = 0;
            this.Camera1Label4.Text = "相机4:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(79, 45);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 1;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(79, 83);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 20);
            this.comboBox2.TabIndex = 1;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(79, 121);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 20);
            this.comboBox3.TabIndex = 1;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(79, 163);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 20);
            this.comboBox4.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.VideoShow);
            this.panel1.Controls.Add(this.StopGraphic);
            this.panel1.Controls.Add(this.StartGraphic);
            this.panel1.Controls.Add(this.OperationCamera);
            this.panel1.Controls.Add(this.SavePicture);
            this.panel1.Controls.Add(this.openCameras);
            this.panel1.Location = new System.Drawing.Point(712, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 146);
            this.panel1.TabIndex = 2;
            // 
            // VideoShow
            // 
            this.VideoShow.Location = new System.Drawing.Point(139, 13);
            this.VideoShow.Name = "VideoShow";
            this.VideoShow.Size = new System.Drawing.Size(75, 23);
            this.VideoShow.TabIndex = 2;
            this.VideoShow.Text = "显示情况";
            this.VideoShow.UseVisualStyleBackColor = true;
            this.VideoShow.Click += new System.EventHandler(this.VideoShow_Click);
            // 
            // StopGraphic
            // 
            this.StopGraphic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.StopGraphic.Location = new System.Drawing.Point(139, 89);
            this.StopGraphic.Name = "StopGraphic";
            this.StopGraphic.Size = new System.Drawing.Size(75, 23);
            this.StopGraphic.TabIndex = 1;
            this.StopGraphic.Text = "停止拍照";
            this.StopGraphic.UseVisualStyleBackColor = true;
            // 
            // StartGraphic
            // 
            this.StartGraphic.Location = new System.Drawing.Point(139, 51);
            this.StartGraphic.Name = "StartGraphic";
            this.StartGraphic.Size = new System.Drawing.Size(75, 23);
            this.StartGraphic.TabIndex = 1;
            this.StartGraphic.Text = "开始拍照";
            this.StartGraphic.UseVisualStyleBackColor = true;
            // 
            // OperationCamera
            // 
            this.OperationCamera.AutoSize = true;
            this.OperationCamera.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.OperationCamera.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.OperationCamera.Location = new System.Drawing.Point(12, 14);
            this.OperationCamera.Name = "OperationCamera";
            this.OperationCamera.Size = new System.Drawing.Size(74, 18);
            this.OperationCamera.TabIndex = 0;
            this.OperationCamera.Text = "相机操作";
            // 
            // SavePicture
            // 
            this.SavePicture.Location = new System.Drawing.Point(34, 89);
            this.SavePicture.Name = "SavePicture";
            this.SavePicture.Size = new System.Drawing.Size(75, 23);
            this.SavePicture.TabIndex = 0;
            this.SavePicture.Text = "拍照保存";
            this.SavePicture.UseVisualStyleBackColor = true;
            this.SavePicture.Click += new System.EventHandler(this.SavePicture_Click);
            // 
            // openCameras
            // 
            this.openCameras.Location = new System.Drawing.Point(34, 51);
            this.openCameras.Name = "openCameras";
            this.openCameras.Size = new System.Drawing.Size(75, 23);
            this.openCameras.TabIndex = 0;
            this.openCameras.Text = "打开相机";
            this.openCameras.UseVisualStyleBackColor = true;
            this.openCameras.Click += new System.EventHandler(this.openCameras_Click);
            // 
            // ChooseCamera
            // 
            this.ChooseCamera.AutoSize = true;
            this.ChooseCamera.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ChooseCamera.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ChooseCamera.Location = new System.Drawing.Point(12, 15);
            this.ChooseCamera.Name = "ChooseCamera";
            this.ChooseCamera.Size = new System.Drawing.Size(74, 18);
            this.ChooseCamera.TabIndex = 0;
            this.ChooseCamera.Text = "相机选择";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.ChooseCamera);
            this.panel2.Controls.Add(this.Camera1Label1);
            this.panel2.Controls.Add(this.comboBox6);
            this.panel2.Controls.Add(this.comboBox4);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.Camera1Label5);
            this.panel2.Controls.Add(this.Camera1Label6);
            this.panel2.Controls.Add(this.Camera1Label3);
            this.panel2.Controls.Add(this.comboBox5);
            this.panel2.Controls.Add(this.Camera1Label4);
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Controls.Add(this.Camera1Label2);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Location = new System.Drawing.Point(712, 164);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(238, 287);
            this.panel2.TabIndex = 2;
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(79, 243);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(121, 20);
            this.comboBox6.TabIndex = 1;
            // 
            // Camera1Label5
            // 
            this.Camera1Label5.AutoSize = true;
            this.Camera1Label5.Location = new System.Drawing.Point(32, 204);
            this.Camera1Label5.Name = "Camera1Label5";
            this.Camera1Label5.Size = new System.Drawing.Size(41, 12);
            this.Camera1Label5.TabIndex = 0;
            this.Camera1Label5.Text = "相机3:";
            // 
            // Camera1Label6
            // 
            this.Camera1Label6.AutoSize = true;
            this.Camera1Label6.Location = new System.Drawing.Point(32, 246);
            this.Camera1Label6.Name = "Camera1Label6";
            this.Camera1Label6.Size = new System.Drawing.Size(41, 12);
            this.Camera1Label6.TabIndex = 0;
            this.Camera1Label6.Text = "相机4:";
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(79, 201);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 20);
            this.comboBox5.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.SetSavePath);
            this.panel3.Controls.Add(this.SavePathText);
            this.panel3.Controls.Add(this.SavePath);
            this.panel3.Location = new System.Drawing.Point(712, 654);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(238, 164);
            this.panel3.TabIndex = 2;
            // 
            // SetSavePath
            // 
            this.SetSavePath.AutoSize = true;
            this.SetSavePath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SetSavePath.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SetSavePath.Location = new System.Drawing.Point(12, 14);
            this.SetSavePath.Name = "SetSavePath";
            this.SetSavePath.Size = new System.Drawing.Size(74, 18);
            this.SetSavePath.TabIndex = 0;
            this.SetSavePath.Text = "设置路径";
            // 
            // SavePathText
            // 
            this.SavePathText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SavePathText.Location = new System.Drawing.Point(34, 97);
            this.SavePathText.Name = "SavePathText";
            this.SavePathText.Size = new System.Drawing.Size(180, 49);
            this.SavePathText.TabIndex = 2;
            this.SavePathText.Text = "C:\\Users\\CVGC\\Pictures";
            // 
            // SavePath
            // 
            this.SavePath.Location = new System.Drawing.Point(58, 55);
            this.SavePath.Name = "SavePath";
            this.SavePath.Size = new System.Drawing.Size(114, 23);
            this.SavePath.TabIndex = 1;
            this.SavePath.Text = "存储路径";
            this.SavePath.UseVisualStyleBackColor = true;
            this.SavePath.Click += new System.EventHandler(this.SavePath_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.SetCamera);
            this.panel4.Controls.Add(this.SetCamera4);
            this.panel4.Controls.Add(this.SetCamera5);
            this.panel4.Controls.Add(this.SetCamera6);
            this.panel4.Controls.Add(this.SetCamera2);
            this.panel4.Controls.Add(this.SetCamera3);
            this.panel4.Controls.Add(this.SetCamera1);
            this.panel4.Location = new System.Drawing.Point(712, 457);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(238, 191);
            this.panel4.TabIndex = 2;
            // 
            // SetCamera
            // 
            this.SetCamera.AutoSize = true;
            this.SetCamera.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SetCamera.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SetCamera.Location = new System.Drawing.Point(12, 18);
            this.SetCamera.Name = "SetCamera";
            this.SetCamera.Size = new System.Drawing.Size(74, 18);
            this.SetCamera.TabIndex = 0;
            this.SetCamera.Text = "相机设置";
            // 
            // SetCamera4
            // 
            this.SetCamera4.Location = new System.Drawing.Point(139, 63);
            this.SetCamera4.Name = "SetCamera4";
            this.SetCamera4.Size = new System.Drawing.Size(75, 23);
            this.SetCamera4.TabIndex = 0;
            this.SetCamera4.Text = "相机4设置";
            this.SetCamera4.UseVisualStyleBackColor = true;
            this.SetCamera4.Click += new System.EventHandler(this.SetCamera4_Click);
            // 
            // SetCamera5
            // 
            this.SetCamera5.Location = new System.Drawing.Point(139, 94);
            this.SetCamera5.Name = "SetCamera5";
            this.SetCamera5.Size = new System.Drawing.Size(75, 23);
            this.SetCamera5.TabIndex = 0;
            this.SetCamera5.Text = "相机5设置";
            this.SetCamera5.UseVisualStyleBackColor = true;
            this.SetCamera5.Click += new System.EventHandler(this.SetCamera5_Click);
            // 
            // SetCamera6
            // 
            this.SetCamera6.Location = new System.Drawing.Point(139, 130);
            this.SetCamera6.Name = "SetCamera6";
            this.SetCamera6.Size = new System.Drawing.Size(75, 23);
            this.SetCamera6.TabIndex = 0;
            this.SetCamera6.Text = "相机6设置";
            this.SetCamera6.UseVisualStyleBackColor = true;
            this.SetCamera6.Click += new System.EventHandler(this.SetCamera6_Click);
            // 
            // SetCamera2
            // 
            this.SetCamera2.Location = new System.Drawing.Point(34, 94);
            this.SetCamera2.Name = "SetCamera2";
            this.SetCamera2.Size = new System.Drawing.Size(75, 23);
            this.SetCamera2.TabIndex = 0;
            this.SetCamera2.Text = "相机2设置";
            this.SetCamera2.UseVisualStyleBackColor = true;
            this.SetCamera2.Click += new System.EventHandler(this.SetCamera2_Click);
            // 
            // SetCamera3
            // 
            this.SetCamera3.Location = new System.Drawing.Point(34, 130);
            this.SetCamera3.Name = "SetCamera3";
            this.SetCamera3.Size = new System.Drawing.Size(75, 23);
            this.SetCamera3.TabIndex = 0;
            this.SetCamera3.Text = "相机3设置";
            this.SetCamera3.UseVisualStyleBackColor = true;
            this.SetCamera3.Click += new System.EventHandler(this.SetCamera3_Click);
            // 
            // SetCamera1
            // 
            this.SetCamera1.Location = new System.Drawing.Point(34, 63);
            this.SetCamera1.Name = "SetCamera1";
            this.SetCamera1.Size = new System.Drawing.Size(75, 23);
            this.SetCamera1.TabIndex = 0;
            this.SetCamera1.Text = "相机1设置";
            this.SetCamera1.UseVisualStyleBackColor = true;
            this.SetCamera1.Click += new System.EventHandler(this.SetCamera1_Click);
            // 
            // RecordText
            // 
            this.RecordText.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.RecordText.Location = new System.Drawing.Point(12, 831);
            this.RecordText.Name = "RecordText";
            this.RecordText.Size = new System.Drawing.Size(940, 161);
            this.RecordText.TabIndex = 3;
            this.RecordText.Text = "";
            this.RecordText.TextChanged += new System.EventHandler(this.RecordText_TextChange);
            // 
            // videoSourcePlayer1
            // 
            this.videoSourcePlayer1.Location = new System.Drawing.Point(12, 12);
            this.videoSourcePlayer1.Name = "videoSourcePlayer1";
            this.videoSourcePlayer1.Size = new System.Drawing.Size(344, 257);
            this.videoSourcePlayer1.TabIndex = 4;
            this.videoSourcePlayer1.Text = "videoSourcePlayer1";
            this.videoSourcePlayer1.VideoSource = null;
            // 
            // videoSourcePlayer2
            // 
            this.videoSourcePlayer2.Location = new System.Drawing.Point(12, 275);
            this.videoSourcePlayer2.Name = "videoSourcePlayer2";
            this.videoSourcePlayer2.Size = new System.Drawing.Size(344, 272);
            this.videoSourcePlayer2.TabIndex = 4;
            this.videoSourcePlayer2.Text = "videoSourcePlayer1";
            this.videoSourcePlayer2.VideoSource = null;
            // 
            // videoSourcePlayer4
            // 
            this.videoSourcePlayer4.Location = new System.Drawing.Point(362, 12);
            this.videoSourcePlayer4.Name = "videoSourcePlayer4";
            this.videoSourcePlayer4.Size = new System.Drawing.Size(344, 257);
            this.videoSourcePlayer4.TabIndex = 4;
            this.videoSourcePlayer4.Text = "videoSourcePlayer1";
            this.videoSourcePlayer4.VideoSource = null;
            // 
            // videoSourcePlayer5
            // 
            this.videoSourcePlayer5.Location = new System.Drawing.Point(362, 275);
            this.videoSourcePlayer5.Name = "videoSourcePlayer5";
            this.videoSourcePlayer5.Size = new System.Drawing.Size(344, 272);
            this.videoSourcePlayer5.TabIndex = 4;
            this.videoSourcePlayer5.Text = "videoSourcePlayer1";
            this.videoSourcePlayer5.VideoSource = null;
            // 
            // Camera1
            // 
            this.Camera1.AutoSize = true;
            this.Camera1.Location = new System.Drawing.Point(306, 252);
            this.Camera1.Name = "Camera1";
            this.Camera1.Size = new System.Drawing.Size(35, 12);
            this.Camera1.TabIndex = 5;
            this.Camera1.Text = "画面1";
            // 
            // Camera2
            // 
            this.Camera2.AutoSize = true;
            this.Camera2.Location = new System.Drawing.Point(306, 526);
            this.Camera2.Name = "Camera2";
            this.Camera2.Size = new System.Drawing.Size(35, 12);
            this.Camera2.TabIndex = 5;
            this.Camera2.Text = "画面2";
            // 
            // Camera3
            // 
            this.Camera3.AutoSize = true;
            this.Camera3.Location = new System.Drawing.Point(652, 249);
            this.Camera3.Name = "Camera3";
            this.Camera3.Size = new System.Drawing.Size(35, 12);
            this.Camera3.TabIndex = 5;
            this.Camera3.Text = "画面4";
            // 
            // Camera4
            // 
            this.Camera4.AutoSize = true;
            this.Camera4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Camera4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Camera4.Location = new System.Drawing.Point(654, 526);
            this.Camera4.Name = "Camera4";
            this.Camera4.Size = new System.Drawing.Size(37, 14);
            this.Camera4.TabIndex = 5;
            this.Camera4.Text = "画面5";
            // 
            // videoSourcePlayer3
            // 
            this.videoSourcePlayer3.Location = new System.Drawing.Point(12, 553);
            this.videoSourcePlayer3.Name = "videoSourcePlayer3";
            this.videoSourcePlayer3.Size = new System.Drawing.Size(344, 272);
            this.videoSourcePlayer3.TabIndex = 4;
            this.videoSourcePlayer3.Text = "videoSourcePlayer1";
            this.videoSourcePlayer3.VideoSource = null;
            // 
            // videoSourcePlayer6
            // 
            this.videoSourcePlayer6.Location = new System.Drawing.Point(362, 553);
            this.videoSourcePlayer6.Name = "videoSourcePlayer6";
            this.videoSourcePlayer6.Size = new System.Drawing.Size(344, 272);
            this.videoSourcePlayer6.TabIndex = 4;
            this.videoSourcePlayer6.Text = "videoSourcePlayer1";
            this.videoSourcePlayer6.VideoSource = null;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(306, 804);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "画面3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(654, 804);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "画面6";
            // 
            // MainWindows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 1004);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Camera4);
            this.Controls.Add(this.Camera3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Camera2);
            this.Controls.Add(this.Camera1);
            this.Controls.Add(this.videoSourcePlayer6);
            this.Controls.Add(this.videoSourcePlayer5);
            this.Controls.Add(this.videoSourcePlayer3);
            this.Controls.Add(this.videoSourcePlayer2);
            this.Controls.Add(this.videoSourcePlayer4);
            this.Controls.Add(this.videoSourcePlayer1);
            this.Controls.Add(this.RecordText);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(980, 1043);
            this.MinimumSize = new System.Drawing.Size(980, 1043);
            this.Name = "MainWindows";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindows_Closing);
            this.Load += new System.EventHandler(this.MainWindows_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Camera1Label1;
        private System.Windows.Forms.Label Camera1Label2;
        private System.Windows.Forms.Label Camera1Label3;
        private System.Windows.Forms.Label Camera1Label4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button SavePicture;
        private System.Windows.Forms.Button openCameras;
        private System.Windows.Forms.Button SavePath;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button SetCamera4;
        private System.Windows.Forms.Button SetCamera2;
        private System.Windows.Forms.Button SetCamera3;
        private System.Windows.Forms.Button SetCamera1;
        private System.Windows.Forms.RichTextBox RecordText;
        private System.Windows.Forms.Label ChooseCamera;
        private System.Windows.Forms.RichTextBox SavePathText;
        private System.Windows.Forms.Label OperationCamera;
        private System.Windows.Forms.Label SetSavePath;
        private System.Windows.Forms.Label SetCamera;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer1;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer2;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer4;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer5;
        private System.Windows.Forms.Label Camera1;
        private System.Windows.Forms.Label Camera2;
        private System.Windows.Forms.Label Camera3;
        private System.Windows.Forms.Label Camera4;
        private System.Windows.Forms.Button StopGraphic;
        private System.Windows.Forms.Button StartGraphic;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer3;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label Camera1Label5;
        private System.Windows.Forms.Label Camera1Label6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Button SetCamera5;
        private System.Windows.Forms.Button SetCamera6;
        private System.Windows.Forms.Button VideoShow;
    }
}

